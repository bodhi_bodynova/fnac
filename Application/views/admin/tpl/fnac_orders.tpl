[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box="list"}]

<div class="container-fluid" style="text-align:center;margin-bottom:5px">
    <img style="width:20%" src="../modules/ewald/fnac/out/img/fnac.png">
</div>
<div class="row">
    <div class="col-5">
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Order ID</th>
                <th scope="col">Bestelldatum</th>
                <th scope="col">Importiert</th>
                <th scope="col">Versandbereit</th>
                <th scope="col">Versendet</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="col-7">
        <div class="card">
            <div class="card-body card-product-data">
                <div class="row">
                    <div class="col-6">

                        <h3>Bestelldetails:</h3>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="orderemail">Order Email</span>
                            </div>
                            <input disabled name="orderemail" type="text" class="form-control background-white" placeholder="" aria-label="orderemail">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="buyerid">Käufer ID</span>
                            </div>
                            <input disabled name="buyerid" type="text" class="form-control background-white" placeholder="" aria-label="buyerid">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="orderid">Order ID</span>
                            </div>
                            <input disabled name="orderid" type="text" class="form-control background-white" placeholder="" aria-label="orderid">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="orderdate">Order Datum</span>
                            </div>
                            <input disabled name="orderdate" type="text" class="form-control background-white" placeholder="" aria-label="orderdate">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="offerid">Angebot ID</span>
                            </div>
                            <input disabled name="offerid" type="text" class="form-control background-white" placeholder="" aria-label="offerid">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="deltimeexpires">Lieferfrist</span>
                            </div>
                            <input disabled name="deltimeexpires" type="text" class="form-control background-white" placeholder="" aria-label="deltimeexpires">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="status">Status</span>
                            </div>
                            <input disabled name="status" type="text" class="form-control background-white" placeholder="" aria-label="status">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="price">Preis</span>
                            </div>
                            <input disabled name="price" type="text" class="form-control background-white" placeholder="" aria-label="price">
                        </div>
                        <h3>Rechnung:</h3>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="billNumber">Rechnungsnummer</span>
                            </div>
                            <input disabled name="billNumber" type="text" class="form-control background-white" placeholder="" aria-label="billNumber">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="billURL">URL</span>
                            </div>
                            <input disabled name="billURL" type="text" class="form-control background-white" placeholder="" aria-label="billURL">
                        </div>

                        <button type="button" class="btn btn-outline-dark" onclick="$('#bestellModal').modal()">Vorgangspositionen</button>

                    </div>


                    <div class="col-6">

                        <h3>Rechnungsanschrift:</h3>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="billVorname">Vorname</span>
                            </div>
                            <input disabled name="billVorname" type="text" class="form-control background-white" placeholder="" aria-label="billVorname">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="billNachname">Nachname</span>
                            </div>
                            <input disabled name="billNachname" type="text" class="form-control background-white" placeholder="" aria-label="billNachname">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="billFirma">Firma</span>
                            </div>
                            <input disabled name="billFirma" type="text" class="form-control background-white" placeholder="" aria-label="billFirma">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="billStrasse">Straße</span>
                            </div>
                            <input disabled name="billStrasse" type="text" class="form-control background-white" placeholder="" aria-label="billStrasse">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="billOrt">PLZ und Ort</span>
                            </div>
                            <input disabled name="billOrt" type="text" class="form-control background-white" placeholder="" aria-label="billOrt">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="billLand">Land</span>
                            </div>
                            <input disabled name="billLand" type="text" class="form-control background-white" placeholder="" aria-label="billLand">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="billTelefon">Telefon</span>
                            </div>
                            <input disabled name="billTelefon" type="text" class="form-control background-white" placeholder="" aria-label="billTelefon">
                        </div>

                        <h3>Lieferanschrift:</h3>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="shipVorname">Vorname</span>
                            </div>
                            <input disabled name="shipVorname" type="text" class="form-control background-white" placeholder="" aria-label="shipVorname">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="shipNachname">Nachname</span>
                            </div>
                            <input disabled name="shipNachname" type="text" class="form-control background-white" placeholder="" aria-label="shipNachname">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="shipFirma">Firma</span>
                            </div>
                            <input disabled name="shipFirma" type="text" class="form-control background-white" placeholder="" aria-label="shipFirma">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="shipStrasse">Straße</span>
                            </div>
                            <input disabled name="shipStrasse" type="text" class="form-control background-white" placeholder="" aria-label="shipStrasse">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="shipOrt">PLZ und Ort</span>
                            </div>
                            <input disabled name="shipOrt" type="text" class="form-control background-white" placeholder="" aria-label="shipOrt">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="shipLand">Land</span>
                            </div>
                            <input disabled name="shipLand" type="text" class="form-control background-white" placeholder="" aria-label="shipLand">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="shipTelefon">Telefon</span>
                            </div>
                            <input disabled name="shipTelefon" type="text" class="form-control background-white" placeholder="" aria-label="shipTelefon">
                        </div>

                    </div>
                </div>
        </div>
    </div>
</div>
[{* MODAL *}]
<div class="modal fade" id="bestellModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Bestellübersicht</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="bestellModalBody">
                <input type="hidden" name="orderunits" value="">
                <input type="hidden" name="tracking" value="">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">ArtikelID</th>
                            <th scope="col">Titel</th>
                            <th scope="col">Menge</th>
                            <th scope="col">Preis</th>
                            <th scope="col">Gesamt</th>
                        </tr>
                    </thead>
                    <tbody id="billTable">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
            </div>
        </div>
    </div>
</div>
[{* MODAL *}]
