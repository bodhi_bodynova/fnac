[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box="list"}]

<div class="container-fluid" style="text-align:center;margin-bottom:5px">
    <img style="width:20%" src="../modules/ewald/fnac/out/img/fnac.png">
</div>
<button type="button" class="btn btn-outline-dark" onclick="testjson()">TestJSON</button>
<div class="container-fluid" >
    <div class="row">
        <div class="col">
            <div class="card bg-light">
                <form action="" method="post">
                    <input type="hidden" name="fnc" value="getCategories">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-3">
                                <h2>Artikel</h2>
                            </div>
                            <div class="col-9">
                                <div class="float-right">
                                    <label for="suche">Suche:</label>
                                    <input id="suche" type="text" name="suche" value="">
                                    <button type="submit" class="btn-xs btn-outline-success">Absenden</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
                <div class="card-body">
                </div>
            </div>
        </div>
        <div class="col" id="productData">
            <div class="card bg-light">
                <div class="card-header">
                </div>
                <div class="card-body card-product-data">
                </div>
            </div>
        </div>
    </div>
</div>
