[{$smarty.block.parent}]

[{if $oView->getClassName() == 'fnac_articles' ||
    $oView->getClassName() == 'fnac_orders' ||
    $oView->getClassName() == 'fnac_offers' ||
    $oView->getClassName() == 'fnac_jsoncontroller'
}]

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script type="text/javascript" src="[{$oViewConf->getModuleUrl('fnac','out/src/js/fnac.js')}]"></script>



<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    function testjson(){

        var res = $.ajax({
            async : false,
            type: "POST",
            url: 'index.php?cl=fnac_jsoncontroller&fnc=testfnac&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            dataType: "json",
            success: function(result){
                console.log(result);
            }
        }).responseJSON;
        console.log(res);
    }
</script>
    [{/if}]