<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 2019-03-07
 * Time: 13:58
 */

$sLangName = "Deutsch";
$iLangNr = 0;
$aLang = array(
    'charset'                                  => 'UTF-8 ',
    'FNAC_MAINMENU'                        => 'FNAC',
    'FNAC_SUBMENU_ARTICLES'                => 'Artikel',
    'FNAC_SUBMENU_ORDERS'                    => 'Bestellungen',
    'FNAC_SUBMENU_OFFERS'                    => 'Angebote',
    'FNAC_SUBMENU_INVOICES'         => 'Rechnungen',
    'FNAC_SUBMENU_INVENTORY'                 => 'Lager',
    'FNAC_SUBMENU_TICKETS'              => 'Tickets',
    'FNAC_SUBMENU_RETURNS'               => 'Retouren',
    'FNAC_SUBMENU_REPORTS'              => 'Reports',
    'GENERAL_OXID'                             => 'OXID',
    'GENERAL_OXPARENTID'                       => 'OXPARENTID'
);