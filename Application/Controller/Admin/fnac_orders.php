<?php


namespace ewald\fnac\Application\Controller\Admin;

use ewald\fnac\Application\Model\fnac_api;
use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\DatabaseProvider;


class fnac_orders extends \OxidEsales\Eshop\Application\Controller\Admin\AdminController

{
    protected $_sClass = 'fnac_orders';

    protected $_sThisTemplate = 'fnac_orders.tpl';

    protected $_aViewData = null;

    protected $_sTestJSON = null;

    public function __construct(){
        $this->_oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $this->_oApi = new fnac_api();
    }

    public function getToken(){
        return Registry::getConfig()->getRequestParameter('stoken');
    }

    public function getAdminSid(){
        return Registry::getConfig()->getRequestParameter('force_admin_sid');
    }

    public function render()
    {
        parent::render();
        return $this->_sThisTemplate;
    }

}