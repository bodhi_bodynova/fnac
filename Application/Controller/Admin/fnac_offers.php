<?php


namespace ewald\fnac\Application\Controller\Admin;


use ewald\fnac\Application\Model\fnac_api;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Core\DatabaseProvider;

class fnac_offers extends \OxidEsales\Eshop\Application\Controller\Admin\AdminController
{

    protected $_sClass = 'fnac_offers';

    protected $_sThisTemplate = 'fnac_offers.tpl';

    protected $_aViewData = null;

    protected $_oDb = null;

    protected $_sSecretKey = null;

    protected $_sClientKey = null;

    protected $_oApi = null;

    public function __construct()
    {
        $this->_oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $this->_oApi = new fnac_api();
    }

    public function render()
    {
        parent::render();
        return $this->_sThisTemplate;
    }

    public function getToken(){
        return Registry::getConfig()->getRequestParameter('stoken');

    }

    public function getAdminSid(){
        return Registry::getConfig()->getRequestParameter('force_admin_sid');
    }

}