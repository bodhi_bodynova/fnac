<?php


namespace ewald\fnac\Application\Controller\Admin;

use ewald\fnac\Application\Model\fnac_api;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Core\DatabaseProvider;

class fnac_articles extends \OxidEsales\Eshop\Application\Controller\Admin\AdminController

{
    protected $_sClass = 'fnac_articles';

    protected $_sThisTemplate = 'fnac_articles.tpl';

    protected $_aViewData = null;

    protected $_sOxid = null;

    protected $_oDb = null;

    protected $_sSecretKey = null;

    protected $_oApi = null;

    protected $_sEan = null;

    /**
     * fnac_articles constructor.
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */

    public function __construct()
    {
        $this->_oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $this->_oApi = new fnac_api();
    }

    public function getToken(){
        return Registry::getConfig()->getRequestParameter('stoken');
    }

    public function getAdminSid(){
        return Registry::getConfig()->getRequestParameter('force_admin_sid');
    }

    public function render()
    {
        parent::render();
        return $this->_sThisTemplate;
    }

    public function getCategories(){
        //$path = '/hierarchies?max_level=2';
        $path = '/products/attributes?max_level=1';
        //$api = new \ewald\fnac\Application\Model\fnac_api();
        $api = new fnac_api();
        $result = $api->getRequest($path);
        echo '<pre>';
        print_r($result);
        die();
    }
}