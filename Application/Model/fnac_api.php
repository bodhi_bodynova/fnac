<?php


namespace ewald\fnac\Application\Model;

use OxidEsales\Eshop\Core\Registry;

/**
 * Diese Klasse ist für alle Funktionen der API-Verbindung verantwortlich. Hier werden sämtliche Anfragen gestellt,
 * als auch eine Funktion zur Verschlüsselung der Daten gestellt.
 * Class fnac_api
 * @package ewald\fnac\Application\Controller\Admin
 */
class fnac_api
{

    protected $_sSecretKey = null;

    protected $_sClientKey = null;

    protected $_sBaseUrl = null;

    public function __construct(){
        $this->_sAPIKey = Registry::getConfig()->getConfigParam('sFNACAPIKey');
        $this->_sBaseUrl = 'https://fnac-prod.mirakl.net/api';
    }


    /**
     * @param $path =>
     * @param string $id
     * @return null
     */
    public function getRequest($path,$id = ''){

        $uri = $this->_sBaseUrl . $path . $id;
        $headers = [
            'Accept: application/json',
            'Authorization: ' . $this->_sAPIKey
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $item = json_decode(curl_exec($ch), true);
        //var_export($item);
        curl_close($ch);
        return $item;
    }

    public function postRequest($path,$data){
        $uri = $this->_sBaseUrl . $path;
        $jsonData = json_encode($data);
        $timestamp = time();

        $headers = [
            'Accept: application/json',
            'Hm-Client: ' . $this->_sClientKey,
            'Hm-Timestamp: ' . $timestamp,
            'Hm-Signature: ' . $this->signRequest('POST', $uri, $jsonData, $timestamp, $this->_sSecretKey),
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);


        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        curl_close($ch);

        return substr($response, $header_size);
    }

    /**
     * @param $path
     * @param $data
     * @return false|string
     */
    public function patchRequest($path,$data){
        $uri = $this->_sBaseUrl . $path;
        $jsonData = json_encode($data);
        $timestamp = time();

        $headers = [
            'Accept: application/json',
            'Hm-Client: ' . $this->_sClientKey,
            'Hm-Timestamp: ' . $timestamp,
            'Hm-Signature: ' . $this->signRequest('PATCH', $uri, $jsonData, $timestamp, $this->_sSecretKey),
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);


        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        curl_close($ch);
        return $response;
        return substr($response, $header_size);
    }

        public function putRequest($path,$data){
        $uri = $this->_sBaseUrl . $path;
        $jsonData = json_encode($data);
        $timestamp = time();
        $headers = [
            'Accept: application/json',
            'Hm-Client: ' . $this->_sClientKey,
            'Hm-Timestamp: ' . $timestamp,
            'Hm-Signature: ' . $this->signRequest('PUT', $uri, $jsonData, $timestamp, $this->_sSecretKey),
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        curl_close($ch);

        return substr($response, $header_size);
    }

    public function deleteRequest($path){
        $uri = $this->_sBaseUrl . $path;

        $timestamp = time();

        $headers = [
            'Accept: application/json',
            'Hm-Client: ' . $this->_sClientKey,
            'Hm-Timestamp: ' . $timestamp,
            'Hm-Signature: ' . $this->signRequest('DELETE', $uri, '', $timestamp, $this->_sSecretKey),
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);

        $response = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return $responseCode;
    }


    /**
     * @param $method => curl Methode
     * @param $uri => URL
     * @param $body => Parameter
     * @param $timestamp
     * @param $secretKey
     * @return string => hashString
     */

    public function signRequest($method, $uri, $body, $timestamp, $secretKey)
    {
        $string = implode("\n", [
            $method,
            $uri,
            $body,
            $timestamp,
        ]);
        return hash_hmac('sha256', $string, $secretKey);
    }

}