<?php
$conf = \OxidEsales\Eshop\Core\Registry::getConfig();
$sModulesDir = $conf->getShopMainUrl().'modules/ewald/fnac/';
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id'          => 'fnac',
    'title'       => [
        'en' => '<img src="'.$sModulesDir.'out/img/favicon.ico" title="wtf">odynova Modul zur FNAC-Verwaltung'
    ],
    'description' => [
        'de' => 'Modul für die Verwaltung der fnac.de-Schnittstelle'
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '1.0',
    'author'      => 'Bodynova GmbH',
    'url'         => 'https://bodynova.de',
    'email'       => 'c.ewald@bodynova.de',

    'extend'      => [
        /*
        \OxidEsales\Eshop\Application\Controller\Admin\ArticleList::class =>
        \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_articles_list::class
        */
    ],
    'controllers'       => array(
        'fnac_api' => \ewald\fnac\Application\Model\fnac_api::class,
        'fnac_articles' => \ewald\fnac\Application\Controller\Admin\fnac_articles::class,
        'fnac_orders' => \ewald\fnac\Application\Controller\Admin\fnac_orders::class,
        'fnac_offers' => \ewald\fnac\Application\Controller\Admin\fnac_offers::class,
        'fnac_jsoncontroller' => \ewald\fnac\Application\Controller\Admin\fnac_jsoncontroller::class
    ),
    'templates'   => array(
        'fnac_articles.tpl' => 'ewald/fnac/Application/views/admin/tpl/fnac_articles.tpl',
        'fnac_orders.tpl' => 'ewald/fnac/Application/views/admin/tpl/fnac_orders.tpl',
        'fnac_offers.tpl' => 'ewald/fnac/Application/views/admin/tpl/fnac_offers.tpl',
        'fnac_json.tpl' => 'ewald/fnac/Application/views/admin/tpl/fnac_json.tpl'
    ),
    'blocks'      => [
        array(
            'template' => 'headitem.tpl',
            'block'=>'admin_headitem_inccss',
            'file'=>'Application/views/admin/blocks/fnac_admin_headitem_inccss.tpl'
        ),
        array(
            'template' => 'headitem.tpl',
            'block'=>'admin_headitem_incjs',
            'file'=>'Application/views/admin/blocks/fnac_admin_headitem_incjs.tpl'
        )
    ],
    'settings'    => [
        [
            'group' => 'fnac_keys',
            'name' => 'sFNACAPIKey',
            'type' => 'str',
            'value' => ''
        ]
    ],
    'events' => [
        /*
        'onActivate' =>
            'Ewald\VisualCMSWidget\Core\Events::onActivate',
        'onDeactivate' =>
            'Ewald\VisualCMSWidget\Core\Events::onDeactivate',
        */
    ],
];